from flask import Flask, request
from flask_restful import Resource, Api
from flask_jwt import JWT, jwt_required
from security import authenticate, identity

app = Flask(__name__)
app.secret_key = "this_is_a_secret_key"
api = Api(app)

jwt = JWT(app, authenticate, identity) # /auth

items = []

class Item(Resource):
    @jwt_required()
    def get(self, _id):
        item = next(filter(lambda i: i['id'] == _id, items), None) # get the first element of the filtered list
        return {'item': item}, 200 if item else 404
    
    @jwt_required()
    def post(self, _id):
        data = request.get_json()
        if next(filter(lambda i: i['id'] == _id or i['name'] == data['name'], items), None):
            return {'message': 'The item already exists'}, 400

        item = {'id': _id, 'name': data['name'], 'price': data['price']}
        items.append(item)
        return item, 201
    
    @jwt_required()
    def delete(self, _id):
        global items
        if len(items) == 0:
            return {'message': 'No items to delete'}
        
        items = list(filter(lambda i: i['id'] != _id, items))
        return {'message': 'Item deleted'}


class ItemList(Resource):
    @jwt_required()
    def get(self):
        return {"items": items}


api.add_resource(Item, '/item/<int:_id>')
api.add_resource(ItemList, '/items')

app.run(port=5000, debug=True)